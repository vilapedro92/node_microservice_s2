const express = require('express');
const router = express.Router();
const precioautomovilesController = require('../controllers/precioAutoController');

router.get('/', precioautomovilesController.getPrecios);

module.exports = router;