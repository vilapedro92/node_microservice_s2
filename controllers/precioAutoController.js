const express = require('express');
const app = express();

const precioautomoviles = require('../models/precioAutoModel');

const precioautomovilesCtrl = {};

//GET

precioautomovilesCtrl.getPrecios =  (req, res) => {
    try{
        const cars =  precioautomoviles;
        res.status(200).json(cars);
    }catch{
        res.status(500).json({message: 'Error in server'});
    }
}

module.exports = precioautomovilesCtrl;

