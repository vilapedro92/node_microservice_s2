const express = require('express');
const app = express();

//Settings
app.set('port', process.env.PORT || 3000);
const precioRoute = require('./routes/routeAutoModel')

//Middlewares
app.use(express.json());

//Routes
app.use('/api/precio', precioRoute);


// Starting the Server
app.listen(app.get('port'), () => {
    console.log('Server on port 3000');
});